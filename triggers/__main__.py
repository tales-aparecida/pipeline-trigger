"""Common main() for all triggers."""
import argparse
import logging

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import gitlab
from cki_lib import misc
from cki_lib.yaml import load
import sentry_sdk

from . import TRIGGERS


def main():
    """Initialize common code and run a specific trigger."""
    misc.sentry_init(sentry_sdk)

    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser(description='Pipeline triggers')
    parser.add_argument(
        '-c', '--config',
        type=str,
        help=(
            'YAML configuration file to use. Will try to use `<name>.yaml` '
            'from the current working directory if not specified, where '
            f'`<name>` is one of: {list(TRIGGERS.keys())}'
        )
    )
    parser.add_argument('trigger',
                        choices=TRIGGERS.keys(),
                        help=('Name of the pipeline trigger'))
    parser.add_argument(
        '--kickstart',
        action='store_true',
        help=('Kickstart the pipelines. Instead of checking for the previously'
              ' existing pipelines, submit the newest available one, without '
              'sending the emails. Note that this created pipeline might be '
              'invalid. Not all triggers support this option.')
    )
    parser.add_argument(
        "--variables",
        action=misc.StoreNameValuePair,
        nargs='+',
        help=(
            'Variables to be added/overriden in all pipelines triggered '
            'e.g. --variables skip_beaker=true --variables mail_to=nobody'
        ),
        default={},
    )
    args = parser.parse_args()

    gitlab_url = misc.get_env_var_or_raise('GITLAB_URL')
    config_path = args.config if args.config else f'{args.trigger}.yaml'

    with open(config_path, encoding='utf-8') as config_file:
        config = load(file_path=config_path,
                      contents=config_file,
                      resolve_references=True,
                      resolve_includes=True)

    gitlab_instance = gitlab.get_instance(gitlab_url)

    if args.trigger == 'brew':
        # This one should run all the time and trigger the pipeline itself
        TRIGGERS[args.trigger].poll_triggers(gitlab_instance,
                                             config)
    else:
        config = config_tree.process_config_tree(config)
        loader = TRIGGERS[args.trigger].load_triggers
        triggers = loader(gitlab_instance, config, args.kickstart)
        cki_pipeline.trigger_multiple(gitlab_instance, triggers,
                                      extra_variables=args.variables)


if __name__ == '__main__':
    main()
