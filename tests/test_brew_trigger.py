"""Tests for triggers.brew_trigger."""
import os
import unittest
from unittest import mock

from cki_lib.misc import utc_now_iso
from freezegun import freeze_time
import yaml

import triggers.brew_trigger as brew


def _prepare(trigger):
    """Prepare a trigger similar to poll_triggers."""
    config = {
        '.amqp': {
            'server_url': 'server_url',
            'web_url': 'web_url',
            'top_url': 'top_url',
        }
    }
    trigger['.extends'] = '.amqp'
    config[trigger.get('name', 'name')] = trigger
    return brew.get_triggers_from_config(config)[0]


class TestPollTriggers(unittest.TestCase):
    """Tests for brew.poll_triggers()."""

    @mock.patch('triggers.brew_trigger.messagequeue.MessageQueue')
    @mock.patch.dict(os.environ, {
        'RABBITMQ_EXCHANGE': 'exchange',
        'RABBITMQ_QUEUE': 'queue',
        'RABBITMQ_ROUTING_KEYS': 'key1 key2',
    })
    def test_trigger_init(self, mock_mq):
        """Verify trigger initialization.

        Verify the configured triggers are cleaned up to only contain strings
        so GitLab accepts them and the container is started.
        """
        config_text = '{}'.format(
            '.amqp:\n'
            '  .cert_path: filepath\n'
            '  .message_topics:\n'
            '    - topic.task.closed\n'
            '  .receiver_urls:\n'
            '    - url:port\n'
            '    - url2:port\n'
            '  server_url: server_url\n'
            '  web_url: web_url\n'
            '  top_url: top_url\n'
            '.amqp091:\n'
            '  .routing_keys:\n'
            '    - org.fedoraproject.prod.buildsys.build.state.change\n'
            '    - org.fedoraproject.prod.buildsys.task.state.change\n'
            '    - org.fedoraproject.prod.copr.build.end\n'
            '  .host: rabbitmq.fedoraproject.org\n'
            '  .port: 5671\n'
            '  .cafile: /etc/cki/umb/fedora-ca.pem\n'
            '  .certfile: /etc/cki/umb/fedora-cert.pem\n'
            '  .virtual_host: /public_pubsub\n'
            '  .exchange: amq.topic\n'
            '.default:\n'
            '  my_var: name@email.org\n'
            'project_name:\n'
            '  .extends: .amqp\n'
            '  cki_project: username/project\n'
            '  rpm_release: fc28\n'
            '  package_name: kernel\n'
            '  cki_pipeline_branch: test_name\n'
            'project_name2:\n'
            '  .extends: .amqp\n'
            '  cki_project: username/project\n'
            '  rpm_release: fc29\n'
            '  package_name: kernel\n'
            '  cki_pipeline_branch: test_name2\n'
        )
        config = yaml.safe_load(config_text)

        mock_foo = mock.Mock()
        mock_mq.return_value = mock_foo

        brew.poll_triggers('gitlab_instance', config)

        mock_foo.consume_messages.assert_called_with(
            'exchange', ['key1', 'key2'], mock.ANY,
            queue_name='queue'
        )


class TestSanityCheck(unittest.TestCase):
    """Tests for brew_trigger.sanity_check()."""

    def test_bad_message(self):
        """Verify sanity check returns False if the new state is not CLOSED."""
        properties = {'new': 'still open'}
        self.assertFalse(brew.sanity_check(properties))

    def test_bad_method(self):
        """Verify sanity check returns False if this is not a build message.

        Check for build method. If that one is not present, we require a
        build_id attribute.
        """
        properties = {'new': 'CLOSED', 'attribute': 'state', 'method': 'nope'}
        self.assertFalse(brew.sanity_check(properties))

        # No method nor build_id
        properties = {'new': 'CLOSED', 'attribute': 'state'}
        self.assertFalse(brew.sanity_check(properties))

    def test_bad_new(self):
        """Verify sanity check returns False if the build is not successful.

        Check for the value of 'new' attribute. For koji, we require 1; for
        brew 'CLOSED' is required.
        """
        properties = {'new': 'FAILED', 'attribute': 'state', 'method': 'build'}
        self.assertFalse(brew.sanity_check(properties))

        properties = {'new': 2, 'attribute': 'state', 'build_id': 111}
        self.assertFalse(brew.sanity_check(properties))

    def test_good_message(self):
        """Verify sanity check returns True if the message is good."""
        properties = {'new': 'CLOSED', 'attribute': 'state', 'method': 'build'}
        self.assertTrue(brew.sanity_check(properties))

        properties = {'new': 'CLOSED', 'attribute': 'state', 'build_id': 111}
        self.assertTrue(brew.sanity_check(properties))


class TestProcessCOPR(unittest.TestCase):
    """Tests for brew_trigger.process_copr()."""

    @freeze_time("2019-01-01")
    def test_pipeline_triggered(self):
        """Check we trigger a pipeline for a valid COPR build."""
        message = {'status': 1,
                   'user': 'user',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30',
                   'build': 123,
                   'chroot': 'fedora-30-x86_64',
                   'owner': 'pkgowner'}
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['pkgowner/repo1', 'pkgowner/repo2'],
                            '.report_rules': [{'if': 'something',
                                               'send_cc': 'someone',
                                               'send_bcc': 'someone_else'}]})

        generated_trigger = brew.process_copr([trigger], message)

        expected_trigger = {
            'rpm_release': 'fc30',
            'package_name': 'kernel',
            'nvr': '{}-{}'.format(message['pkg'], message['version']),
            'name': 'name',
            'title': 'COPR: {}-{}: x86_64'.format(message['pkg'],
                                                  message['version']),
            'owner': 'user',
            'architectures': 'x86_64',
            'repo_name': 'pkgowner/repo1',
            'copr_build': '123',
            'discovery_time': utc_now_iso(),
            'server_url': 'server_url',
            'web_url': 'web_url',
            'top_url': 'top_url',
            'report_rules': (
                '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]'
            )
        }
        self.assertEqual(expected_trigger, generated_trigger)

    def test_bad_status(self):
        """Verify we don't run the pipeline for unsuccessful builds."""
        message = {'status': 2,
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr([], message)
            self.assertIn(f'COPR build for {nvr} not successful',
                          log.output[-1])

    def test_no_pkg_version(self):
        """Verify we return right after detecting pkg or version is missing."""
        message = {}
        generated_trigger = brew.process_copr([], message)
        self.assertIsNone(generated_trigger)

        # Still missing version
        message = {'pkg': 'something'}
        generated_trigger = brew.process_copr([], message)
        self.assertIsNone(generated_trigger)

    def test_no_pipeline(self):
        """Check we don't trigger a pipeline for non-configured builds."""
        message = {'status': 1,
                   'owner': 'different-owner',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['owner/repo1', 'owner/repo2']})

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr([trigger], message)
            self.assertIn(f'COPR: Pipeline for {nvr} not configured',
                          log.output[-1])

    def test_bad_package_name(self):
        """Check we don't continue if the package name doesn't match."""
        message = {'status': 1,
                   'owner': 'owner',
                   'copr': 'repo1',
                   'pkg': 'python-metakernel',
                   'version': '0.21-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['owner/repo1', 'owner/repo2']})

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr([trigger], message)
            self.assertIn(f'COPR: Pipeline for {nvr} not configured',
                          log.output[-1])


class TestAMQPMessageReceiver(unittest.TestCase):
    """Test AMQPMessageReceiver methods."""

    @staticmethod
    def test_callback_fedmsg():
        """Test callback with message from fedmsg."""
        receiver = brew.AMQPMessageReceiver(None, None, None)
        receiver.process_fedmsg = mock.Mock()

        body = {'foo': 'bar'}
        headers = {'message-amqp-bridge-name': 'fedmsg'}

        receiver.callback(body, headers)
        receiver.process_fedmsg.assert_called_with(body)

    @staticmethod
    def test_callback_umb():
        """Test callback with message from umb."""
        receiver = brew.AMQPMessageReceiver(None, None, None)
        receiver.process_umb = mock.Mock()

        body = {'foo': 'bar'}
        headers = {'message-amqp-bridge-name': 'umb'}

        receiver.callback(body, headers)
        receiver.process_umb.assert_called_with(body, headers)

    def test_callback_unknown(self):
        """Test callback with message unknown."""
        receiver = brew.AMQPMessageReceiver(None, None, None)

        body = {'foo': 'bar'}
        headers = {'message-amqp-bridge-name': 'foobar'}

        self.assertRaises(Exception, receiver.callback, body, headers)

    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=False))
    def test_process_fedmsg_sanity_bad(self):
        # pylint: disable=protected-access
        """Test process_fedmsg. Sanity check failed."""
        receiver = brew.AMQPMessageReceiver(None, None, None)
        receiver._process_message = mock.Mock()

        body = {'foo': 'bar'}

        receiver.process_fedmsg(body)
        self.assertFalse(receiver._process_message.called)

    @staticmethod
    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    def test_process_fedmsg_info(mock_process_message):
        # pylint: disable=protected-access
        """Test process_fedmsg. Body is replaced with the value of info."""
        receiver = brew.AMQPMessageReceiver(None, None, None)

        body = {'info': 'bar'}

        receiver.process_fedmsg(body)
        mock_process_message.assert_called_with(None, '.amqp091', 'bar')

    @staticmethod
    @mock.patch('triggers.brew_trigger.sanity_check', mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    def test_process_fedmsg(mock_process_message):
        # pylint: disable=protected-access
        """Test process_fedmsg."""
        receiver = brew.AMQPMessageReceiver(None, None, None)

        body = {'foo': 'bar'}

        receiver.process_fedmsg(body)
        mock_process_message.assert_called_with(None, '.amqp091', body)

    @staticmethod
    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    def test_process_umb_info(mock_process_message):
        # pylint: disable=protected-access
        """Test process_umb. Body is replaced with the value of info."""
        receiver = brew.AMQPMessageReceiver(None, None, None)

        body = {'info': 'bar'}
        headers = {'message-amqp10-properties': 'foobar'}

        receiver.process_umb(body, headers)
        mock_process_message.assert_called_with(None, '.amqp', 'bar')

    @staticmethod
    @mock.patch('triggers.brew_trigger.sanity_check', mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    def test_process_umb(mock_process_message):
        # pylint: disable=protected-access
        """Test process_umb."""
        receiver = brew.AMQPMessageReceiver(None, None, None)

        body = {'foo': 'bar'}
        headers = {'message-amqp10-properties': 'foobar'}

        receiver.process_umb(body, headers)
        mock_process_message.assert_called_with(None, '.amqp', body)

    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=False))
    def test_process_umb_sanity_bad(self):
        # pylint: disable=protected-access
        """Test process_umb. Sanity check failed."""
        receiver = brew.AMQPMessageReceiver(None, None, None)
        receiver._process_message = mock.Mock()

        body = {'foo': 'bar'}
        headers = {'message-amqp10-properties': 'foobar'}

        receiver.process_umb(body, headers)
        self.assertFalse(receiver._process_message.called)

    @mock.patch('triggers.brew_trigger.process_copr')
    @mock.patch('triggers.brew_trigger.trigger_pipeline')
    def test_callback_copr(self, mock_trigger, mock_process):
        # pylint: disable=protected-access
        """Test callback COPR message."""
        mock_process.return_value = 'foo'
        receiver = brew.AMQPMessageReceiver('trigger', 'brew_config', 'gitlab_instance')

        message = {'copr': 'foo'}
        headers = {'message-amqp-bridge-name': 'fedmsg'}
        receiver.callback(message, headers)

        self.assertTrue(mock_process.called)
        mock_process.assert_called_with('trigger', message)

        self.assertTrue(mock_trigger.called)
        mock_trigger.assert_called_with('gitlab_instance', 'foo')

    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    @mock.patch('triggers.brew_trigger.trigger_pipeline')
    def test_callback_message(self, mock_trigger, mock_process):
        # pylint: disable=protected-access
        """Test callback. Result message."""
        mock_process.return_value = 'foo'
        receiver = brew.AMQPMessageReceiver('trigger', 'brew_config', 'gitlab_instance')

        message = {'not-copr-nor-cki_pipeline_id': 'foo'}
        headers = {'message-amqp-bridge-name': 'fedmsg'}
        receiver.callback(message, headers)

        self.assertTrue(mock_process.called)
        mock_process.assert_called_with('brew_config', '.amqp091', message)

        self.assertTrue(mock_trigger.called)
        mock_trigger.assert_called_with('gitlab_instance', 'foo')

    @mock.patch('triggers.brew_trigger.sanity_check',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.process_message')
    @mock.patch('triggers.brew_trigger.trigger_pipeline')
    def test_callback_no_trigger(self, mock_trigger, mock_process):
        # pylint: disable=protected-access
        """Test callback. No pipeline to trigger."""
        mock_process.return_value = None
        receiver = brew.AMQPMessageReceiver('trigger', 'brew_config', 'gitlab_instance')

        message = {'not-copr-nor-cki_pipeline_id': 'foo'}
        headers = {'message-amqp-bridge-name': 'fedmsg'}
        receiver.callback(message, headers)

        self.assertTrue(mock_process.called)
        mock_process.assert_called_with('brew_config', '.amqp091', message)

        self.assertFalse(mock_trigger.called)

    @staticmethod
    @mock.patch('triggers.brew_trigger.misc.is_production',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.cki_pipeline.trigger_multiple')
    def test_trigger_pipeline(mock_trigger):
        """Test trigger_pipeline."""
        brew.trigger_pipeline('gitlab', 'trigger')
        mock_trigger.assert_called_with('gitlab', ['trigger'])

    @mock.patch('triggers.brew_trigger.misc.is_production',
                mock.Mock(return_value=False))
    @mock.patch('triggers.brew_trigger.cki_pipeline.trigger_multiple')
    def test_trigger_pipeline_staging(self, mock_trigger):
        """Test trigger_pipeline. Don't trigger on staging."""
        with self.assertLogs(level='INFO', logger=brew.LOGGER) as log:
            brew.trigger_pipeline('gitlab', 'trigger')
            self.assertIn('Staging environment, not triggering pipeline.',
                          log.output[-1])

        self.assertFalse(mock_trigger.called)

    @mock.patch('triggers.brew_trigger.misc.is_production',
                mock.Mock(return_value=True))
    @mock.patch('triggers.brew_trigger.cki_pipeline.trigger_multiple')
    def test_trigger_pipeline_fail(self, mock_trigger):
        """Test trigger_pipeline. Exception is logged and raised."""
        mock_trigger.side_effect = Exception

        with self.assertLogs(level='INFO', logger=brew.LOGGER) as log:
            self.assertRaises(Exception, brew.trigger_pipeline, 'gitlab', 'trigger_123')
            self.assertIn('Unable to trigger pipeline for trigger_123',
                          log.output[-1])
