"""Tests for triggers.baseline_trigger."""
import os
import unittest
from unittest import mock

from cki_lib import config_tree
from cki_lib.misc import utc_now_iso
from freezegun import freeze_time
import yaml

from tests import fakes
import triggers.baseline_trigger as baseline


class TestLoadTriggers(unittest.TestCase):
    """Tests for baseline.load_triggers()."""

    def test_set_values(self):
        """Verify the config is valid."""
        self._test_set_values('cki_project', 'username/project')

    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'username'})
    def test_set_values_pp(self):
        """GITLAB_PARENT_PROJECT: Verify the config is valid."""
        self._test_set_values('cki_pipeline_project', 'project')

    @freeze_time("2019-01-01")
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.utils.was_tested')
    def _test_set_values(self, key, value, mock_tested, mock_commit):
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  .branches:\n'
            '    - main\n'
            f'  {key}: {value}\n'
            '  cki_pipeline_branch: test_name\n'
            '  .report_rules:\n'
            '    - if: something\n'
            '      send_cc: someone\n'
            '      send_bcc: someone_else\n'
        )
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tested.return_value = False
        mock_commit.side_effect = ['current_baseline', 'current_baseline']

        expected_trigger = {
            'git_url': 'http://git.test/git/kernel.git',
            'watch_url': 'http://git.test/git/kernel.git',
            'name': 'test_name',
            'commit_hash': 'current_baseline',
            'watched_repo_commit_hash': 'current_baseline',
            'branch': 'main',
            'watch_branch': 'main',
            'cki_pipeline_branch': 'test_name',
            'cki_project': 'username/project',
            'title': 'Baseline: test_name main:current_base',
            'require_manual_review': True,
            'discovery_time': utc_now_iso(),
            'report_rules': (
                '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]'
            )
        }

        triggers = baseline.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [expected_trigger])

    def test_already_triggered(self):
        """Verify pipeline are not duplicated."""
        self._test_already_triggered('cki_project', 'username/project')

    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'username'})
    def test_already_triggered_pp(self):
        """GITLAB_PARENT_PROJECT: Verify pipeline are not duplicated."""
        self._test_already_triggered('cki_pipeline_project', 'project')

    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.utils.was_tested')
    def _test_already_triggered(self, key, value, mock_tested, mock_commit):
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  .branches:\n'
            '    - main\n'
            f'  {key}: {value}\n'
            '  cki_pipeline_branch: test_name\n'
        )
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tested.return_value = True
        mock_commit.return_value = 'current_baseline'

        triggers = baseline.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [])
